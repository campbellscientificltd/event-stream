# event-stream

## What is it

Provides a simplified and consistent api for interacting with the event-stream that allows microservices to interact with one another.

## TODOs

- Create a custom error object, so apps using this package can easily distinguish when the error has come from the event stream.

## Logic

I've tried to keep the external api here as generic as possible so that if I ever change from using **RabbitMQ** as my messaging service I won't need to change the code that calls this module, as it will still have the same generic .init(), .publish() and .subscribe() interface. Therefore if we switch from using RabbitMQ then this is the only file that needs changing. 

Uses a pub/sub setup with RabbitMQ, with messages published to a fanout exchange, to which other apps can subscribe to, i.e. create their own queue for (which will be bound to the exchange). The queue each subscribing app creates will include the name of the app itself so that if there are multiple instances of the same app they will all pull from the same queue.


## Repository

Make sure when you commit it up to bitbucket that you also include the git tags, i.e. that ```npm version patch/minor/major``` adds, otherwise dependent apps won't be able to use the #semver installs.

You can push with tags in VS Code by searching for **Git Push (Follow Tags)** in the command pallete, or upload all tags from the command line with ```git push origin --tags```. 


## Using it in another Node.js App

Install it in your other app using the following:

```
npm install bitbucket:campbellscientificltd/event-stream#semver:^1.0.0
```

which will add the following to your _package.json_ file:

```json
{
  "event-stream": "bitbucket:campbellscientificltd/event-stream#semver:^1.0.0",
}
```

It's installed into your node_modules directory and can be used just like a module that has come from npm, i.e:

```js
const event = require('event-stream');
```

const event = require('../index');
const logger = require('node-logger');

logger.configure({level: 'debug', enabled: true, format: 'terminal'});

const url = 'amqp://localhost';
const appName = 'test-event-stream-app';


event.init({url, appName})
.then(() => {
  logger.debug('Initialisation ok');
  createSomeSubscriptions();
  tryPublishing();
})
.catch((err) => {
  logger.error('Error during event-stream initialisation', err);
  // Let's add the subscriptions even if the init failed (e.g. because RabbitMQ wasn't turned on yet), this ensures the subscriptions get added to the list and will be automatically re-established if the connection returns.  
  createSomeSubscriptions();
});



function tryPublishing() {

  event.publish('some-test-publish-event', `Hello there the time is ${new Date().toISOString()}`)
  .then((result) => {
    logger.debug('Published ok');
  })
  .catch((err) => {
    logger.debug('Failed to publish', err);
  });

}



function createSomeSubscriptions() {

  event.subscribe('some-test-subscribe-event', (message) => {

    logger.debug(`New 'some-test-subscribe-event' event`, message);
    
  })
  .then(() => {
    logger.debug(`Subscribed to 'some-test-subscribe-event' events`);
  })
  .catch((err) => {
    logger.error(`Failed to subscribe to 'some-test-subscribe-event' event`, err);
  });

}